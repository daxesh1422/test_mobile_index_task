import 'package:flutter/cupertino.dart';
import 'package:test_mobile_index/core/responsive_ui/responsive_wrapper.dart';


abstract class ResponsiveStatelessWidget extends StatelessWidget {
  const ResponsiveStatelessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => _getScreenSpecificWidget(context) ??
        buildExpandedUI(context) ??
        buildMediumUI(context) ??
        buildCompactUI(context) ??
        emptyWidget();


  Widget? buildExpandedUI(BuildContext context) => null;

  Widget? buildMediumUI(BuildContext context) => null;

  Widget? buildCompactUI(BuildContext context) => null;

  Widget emptyWidget() => Container(
        alignment: Alignment.center,
        child: const Text("No UI found for this element"),
      );

  Widget? _getScreenSpecificWidget(BuildContext context) =>
      ResponsiveWrapper.of(context).isDesktop
          ? buildExpandedUI(context)
          : ResponsiveWrapper.of(context).isTablet
              ? buildMediumUI(context)
              : ResponsiveWrapper.of(context).isMobile
                  ? buildCompactUI(context)
                  : null;
}
