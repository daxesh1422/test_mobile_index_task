import 'package:flutter/cupertino.dart';
import 'package:test_mobile_index/core/responsive_ui/responsive_wrapper.dart';


abstract class ResponsiveWidgetState<T extends StatefulWidget>
    extends State<T> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => _getScreenSpecificWidget() ??
        buildExpandedUI() ??
        buildMediumUI() ??
        buildCompactUI() ??
        emptyWidget();

  /// for desktop screens
  Widget? buildExpandedUI() => null;

  /// for tablet screens
  Widget? buildMediumUI() => null;

  /// for mobile screens
  Widget? buildCompactUI() => null;

  Widget emptyWidget() => Container(
        alignment: Alignment.center,
        child: const Text("No UI found for this element"),
      );

  Widget? _getScreenSpecificWidget() => ResponsiveWrapper.of(context).isDesktop
      ? buildExpandedUI()
      : ResponsiveWrapper.of(context).isTablet
          ? buildMediumUI()
          : ResponsiveWrapper.of(context).isMobile
              ? buildCompactUI()
              : null;
}
