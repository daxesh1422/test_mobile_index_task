import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';

final sharedSL = GetIt.instance;

Future<void> initServiceLocator() async {
  /// logger
  sharedSL.registerSingleton<Logger>(Logger());

}
