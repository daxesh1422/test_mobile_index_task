import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_mobile_index/core/responsive_ui/responsive_wrapper.dart';
import 'package:test_mobile_index/core/responsive_ui/stateful_responsive_state.dart';

abstract class BaseScaffoldScreenState<T extends StatefulWidget>
    extends ResponsiveWidgetState<T> {
  RxBool isFetching = false.obs;
  RxBool showLoader = false.obs;
  RxBool isLoad = true.obs;

  Widget? get fab => null;
  bool handleWillPopScope = false;

  int _horizontalPaddingFlex = 0;

  bool showAppBar = true;
  bool centerAppBarTitle = false;
  bool hasHorizontalPadding = true;

  /// used to add more children in stack widget which is used as a base widget
  /// for scaffold body.
  RxList layers = <Widget>[].obs;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget buildExpandedUI() {
    _horizontalPaddingFlex = hasHorizontalPadding ? 0 : 0;
    return buildScaffold();
  }

  @override
  Widget buildMediumUI() {
    _horizontalPaddingFlex = hasHorizontalPadding ? 0 : 0;
    return buildScaffold();
  }

  @override
  Widget buildCompactUI() {
    _horizontalPaddingFlex = hasHorizontalPadding ? 0 : 0;
    return buildScaffold();
  }

  Widget buildScaffold() {
    var w = Scaffold(
      floatingActionButton: fab,
      appBar: showAppBar ? getAppBar() : null,
      body: Obx(() => (isFetching.value)
          ? loaderWidget()
          : Stack(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(flex: _horizontalPaddingFlex, child: Container()),
                    Expanded(
                        flex: 30,
                        child: _getScreenSpecificBodyWidget() ??
                            buildExpandedBodyWidget() ??
                            buildMediumBodyWidget() ??
                            buildCompactBodyWidget() ??
                            emptyWidget()),
                    Expanded(flex: _horizontalPaddingFlex, child: Container()),
                  ],
                ),
                if ((layers).isNotEmpty) ...layers.map((e) => e).toList(),
                Obx(() => (showLoader.value) ? loaderWidget() : Container())
              ],
            )),
      bottomNavigationBar: getBottomNavigation(),
    );

    return handleWillPopScope
        ? WillPopScope(onWillPop: onWillPop, child: w)
        : w;
  }

  String getAppTitle() => '';

  List<Widget> getActions() => [];

  Future<bool> onWillPop() => Future.value(true);

  Widget? buildExpandedBodyWidget() => null;

  Widget? buildMediumBodyWidget() => null;

  Widget? buildCompactBodyWidget() => null;

  Widget? _getScreenSpecificBodyWidget() =>
      ResponsiveWrapper.of(context).isDesktop
          ? buildExpandedBodyWidget()
          : ResponsiveWrapper.of(context).isTablet
              ? buildMediumBodyWidget()
              : ResponsiveWrapper.of(context).isMobile
                  ? buildCompactBodyWidget()
                  : null;

  Widget? getBottomNavigation() => null;

  set horizontalPaddingFlex(int value) => _horizontalPaddingFlex = value;

  PreferredSizeWidget getAppBar() => AppBar(
        centerTitle: centerAppBarTitle,
        title: getAppBarTitleWidget(),
        actions: [...getActions()],
      );

  Widget getAppBarTitleWidget() => Text(getAppTitle());

  Widget loaderWidget() => const Center(
        child: CircularProgressIndicator(),
      );
}
