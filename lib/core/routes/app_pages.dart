import 'package:get/get.dart';
import 'package:test_mobile_index/ui/readme/screen/readme.dart';
import 'package:test_mobile_index/ui/registration/screen/registration.dart';

part 'app_routes.dart';

class AppPages {
  ///This is the route which is opened when the app starts
  static const initial = Routes.root;

  ///Use this route to redirect user to starting point of the app, for example after password reset.
  static const redirectHome = Routes.root;

  ///Contains all the available routes for the application
  static final routes = [
    GetPage(name: Routes.root, page: () => const ReadMeScreen()),
    GetPage(name: Routes.registration, page: () => const RegistrationScreen()),
  ];
}
