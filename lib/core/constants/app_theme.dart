import 'package:flutter/material.dart';
import 'package:test_mobile_index/core/constants/styles.dart';
import 'package:test_mobile_index/core/constants/text_styles.dart';
import 'colors_helper.dart';

enum ThemeType { light, dark }

class AppTheme {
  final ThemeType themType;

  static const int _appPrimaryColorValue = 0xFF3F73B8;
  static const int _appSecondaryColorValue = 0xFF48BDD0;
  static const int _appGrayColorValue = 0xFF8D959D;

  Color primary;
  Color accent;

  Color primaryBg; // will be used for widgets bg color
  Color secondary;
  Color grey;
  Color error;
  Color warning;
  Color success;
  MaterialColor primaryMaterialColor;
  MaterialColor secondaryMaterialColor;
  MaterialColor greyMaterialColor;
  LinearGradient gradientLight;
  LinearGradient gradientDark;
  bool isWidescreen;

  /// Default constructor
  AppTheme(this.themType,
      {
      required this.primaryBg,
      required this.primary,
      required this.secondary,
      required this.accent,
      required this.error,
      required this.grey,
      required this.warning,
      required this.success,
      required this.primaryMaterialColor,
      required this.secondaryMaterialColor,
      required this.greyMaterialColor,
      required this.gradientLight,
      required this.gradientDark,
      this.isWidescreen = true});

  factory AppTheme.light({bool isWidescreen = true}) => AppTheme(
        ThemeType.light,

        primaryBg: const Color(0xFFF8F9FA),
        primary: const Color(_appPrimaryColorValue),
        secondary: const Color(_appSecondaryColorValue),
        accent: const Color(_appSecondaryColorValue),
        grey: const Color(_appGrayColorValue),
        error: const Color(0xffEF5B5B),
        warning: const Color(0xFFFFBA49),
        success: const Color(0xFF20A38D),
        isWidescreen: isWidescreen,
        primaryMaterialColor: const MaterialColor(
          _appPrimaryColorValue,
          <int, Color>{
            50: Color(0xFFDCE6F2),
            100: Color(0xFFD9E3F1),
            200: Color(0xFFB2C7E3),
            300: Color(0xFF8CABD4),
            400: Color(0xFF658FC6),
            500: Color(_appPrimaryColorValue),
            600: Color(0xFF325C93),
            700: Color(0xFF26456E),
            800: Color(0xFF192E4A),
            900: Color(0xFF0D1725),
          },
        ),
        secondaryMaterialColor: const MaterialColor(
          _appSecondaryColorValue,
          <int, Color>{
            50: Color(0xFFEEF9FB),
            100: Color(0xFFDAF2F6),
            200: Color(0xFFB6E5EC),
            300: Color(0xFF91D7E3),
            400: Color(0xFF6DCAD9),
            500: Color(_appSecondaryColorValue),
            600: Color(0xFF3A97A6),
            700: Color(0xFF2B717D),
            800: Color(0xFF1D4C53),
            900: Color(0xFF0E262A),
          },
        ),
        greyMaterialColor: const MaterialColor(
          _appGrayColorValue,
          <int, Color>{
            50: Color(0xFFF8F9FA),
            100: Color(0xFFE9ECEF),
            200: Color(0xFFDEE2E6),
            300: Color(0xFFCED4DA),
            400: Color(0xFFADB5BD),
            500: Color(_appGrayColorValue),
            600: Color(0xFF6C757D),
            700: Color(0xFF495057),
            800: Color(0xFF343A40),
            900: Color(0xFF212529),
          },
        ),
        gradientLight: const LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFF3F73B8),
            Color(0xFF48BDD0),
          ],
        ),
        gradientDark: const LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Color(0xFF4391C2),
            Color(0xFF3F73B8),
          ],
        ),
      );



  ThemeData get themeData {
    //Add fields here which you want to apply to Theme.of(context) based on theme type
    return ThemeData(
      primarySwatch: primaryMaterialColor,
      textTheme: getTextTheme(isWidescreen),
      elevatedButtonTheme: defaultElevatedButtonThemeData,
      outlinedButtonTheme: defaultOutlineButtonThemeData,
      snackBarTheme: defaultSnackBarThemeData,
      iconTheme: defaultIconThemeData,
    );
  }

  /// for get text theme acc to platform
  TextTheme getTextTheme(bool isWidescreen) {
    return isWidescreen
        ? const TextTheme(
            headline1: AppTextStyles.headline1Web,
            headline2: AppTextStyles.headline2Web,
            headline3: AppTextStyles.headline3Web,
            headline4: AppTextStyles.headline4Web,
            headline5: AppTextStyles.headline5Web,
            headline6: AppTextStyles.headline6Web,
            subtitle1: AppTextStyles.subtitle1Web,
            subtitle2: AppTextStyles.subtitle2Web,
            bodyText1: AppTextStyles.bodyText1Web,
            bodyText2: AppTextStyles.bodyText2Web,
            button: AppTextStyles.buttonWeb,
            caption: AppTextStyles.captionWeb,
            overline: AppTextStyles.overLineWeb)
        : const TextTheme(
            headline1: AppTextStyles.headline1Mobile,
            headline2: AppTextStyles.headline2Mobile,
            headline3: AppTextStyles.headline3Mobile,
            headline4: AppTextStyles.headline4Mobile,
            headline5: AppTextStyles.headline5Mobile,
            headline6: AppTextStyles.headline6Mobile,
            subtitle1: AppTextStyles.subtitle1Mobile,
            subtitle2: AppTextStyles.subtitle2Mobile,
            bodyText1: AppTextStyles.bodyText1Mobile,
            bodyText2: AppTextStyles.bodyText2Mobile,
            button: AppTextStyles.buttonMobile,
            caption: AppTextStyles.captionMobile,
            overline: AppTextStyles.overLineMobile);
  }

  static ElevatedButtonThemeData defaultElevatedButtonThemeData =
      ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Corners.s8),
      ),
      textStyle: AppTextStyles.appButton,
      minimumSize: const Size(150, 45),
      primary: ColorsHelper.primaryColor,
      onPrimary: Colors.white,
    ),
  );

  static OutlinedButtonThemeData defaultOutlineButtonThemeData =
      OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      textStyle: AppTextStyles.appButton,
      minimumSize: const Size(150, 45),
      primary: ColorsHelper.primaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Corners.s8),
      ),
      side: BorderSide(width: Corners.s2, color: ColorsHelper.primaryColor),
    ),
  );

  static SnackBarThemeData defaultSnackBarThemeData = SnackBarThemeData(
    backgroundColor: ColorsHelper.primaryColor,
  );

  static IconThemeData defaultIconThemeData =
      const IconThemeData(color: Colors.white);

  Color get primaryExtraLight => primaryMaterialColor.shade200;

  Color get primaryLight => primaryMaterialColor.shade400;

  Color get primaryDark => primaryMaterialColor.shade600;

  Color get primaryDarker => primaryMaterialColor.shade700;

  Color get greyStrong => greyMaterialColor.shade600;

  Color get greyWeak => greyMaterialColor.shade400;

  Color get fixedLightColor => Colors.white;

  Color get fixedDarkColor => Colors.black;

  Color get lightColor => Colors.white; // will be changed acc to app theme

  Color get darkColor => Colors.black; // will be changed acc to app theme
}
