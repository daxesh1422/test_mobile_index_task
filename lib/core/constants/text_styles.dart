import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_mobile_index/core/constants/colors_helper.dart';

class AppTextStyles {
  /// Global text styles for WEB platform
  static const headline1Web = TextStyle(
    fontSize: 96,
    fontWeight: FontWeight.w200,
  );

  static const headline2Web =
      TextStyle(fontSize: 60, fontWeight: FontWeight.w300);

  static const headline3Web =
      TextStyle(fontSize: 48, fontWeight: FontWeight.w400);

  static const headline4Web =
      TextStyle(fontSize: 32, fontWeight: FontWeight.w600);

  static const headline5Web = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w400,
  );

  static const headline6Web =
      TextStyle(fontSize: 20, fontWeight: FontWeight.w500);

  static const subtitle1Web =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w500);

  static const subtitle2Web =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w500);

  static const bodyText1Web =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w400);

  static const bodyText2Web =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w400);

  static const buttonWeb = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );

  static const captionWeb =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w500);

  static const overLineWeb =
      TextStyle(fontSize: 10, fontWeight: FontWeight.w500);

  /// Global text styles for MOBILE platform
  static const headline1Mobile =
      TextStyle(fontSize: 40, fontWeight: FontWeight.w300);

  static const headline2Mobile =
      TextStyle(fontSize: 32, fontWeight: FontWeight.w300);

  static const headline3Mobile =
      TextStyle(fontSize: 28, fontWeight: FontWeight.w400);

  static const headline4Mobile =
      TextStyle(fontSize: 24, fontWeight: FontWeight.w500);

  static const headline5Mobile = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w400,
  );

  static const headline6Mobile =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w500);

  static const subtitle1Mobile =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w500);

  static const subtitle2Mobile =
      TextStyle(fontSize: 12, fontWeight: FontWeight.w500);

  static const bodyText1Mobile =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w400);

  static const bodyText2Mobile =
      TextStyle(fontSize: 12, fontWeight: FontWeight.w400);

  static const buttonMobile = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );

  static const captionMobile =
      TextStyle(fontSize: 12, fontWeight: FontWeight.w400);

  static const overLineMobile =
      TextStyle(fontSize: 10, fontWeight: FontWeight.w500);

  static const dialogTitle =
      TextStyle(fontSize: 18, fontWeight: FontWeight.w700);

  /// default text styles
  static TextStyle get h1Default => Get.find<ThemeData>().textTheme.headline1!;

  static TextStyle get h2Default => Get.find<ThemeData>().textTheme.headline2!;

  static TextStyle get h3Default => Get.find<ThemeData>().textTheme.headline3!;

  static TextStyle get h4Default => Get.find<ThemeData>().textTheme.headline4!;

  static TextStyle get h5Default => Get.find<ThemeData>().textTheme.headline5!;

  static TextStyle get h6Default => Get.find<ThemeData>().textTheme.headline6!;

  static TextStyle get subTitle1Default =>
      Get.find<ThemeData>().textTheme.subtitle1!;

  static TextStyle get subTitle2Default =>
      Get.find<ThemeData>().textTheme.subtitle2!;

  static TextStyle get bodyText1Default =>
      Get.find<ThemeData>().textTheme.bodyText1!;

  static TextStyle get bodyText2Default =>
      Get.find<ThemeData>().textTheme.bodyText2!;

  static TextStyle get buttonDefault => Get.find<ThemeData>().textTheme.button!;

  static TextStyle get captionDefault =>
      Get.find<ThemeData>().textTheme.caption!;

  static TextStyle get overLineDefault =>
      Get.find<ThemeData>().textTheme.overline!;

  /// used for elevated & outlined button text styles
  static TextStyle get appButton =>
      const TextStyle(fontSize: 18, fontWeight: FontWeight.w500);

  static TextStyle get readMeTextStyle => const TextStyle(

    color:ColorsHelper. readMeTextColor,
    fontSize: 21,
  );


}
