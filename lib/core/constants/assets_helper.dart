class AssetsHelper {
  static String imgAboutMe = 'assets/images/about_me.png';
  static String imgAgreement = 'assets/images/agreement.png';
  static String imgBusinessDeal = 'assets/images/business_deal.png';
  static String imgJobOffers = 'assets/images/job_offers.png';
  static String imgLeftToRightArrow = 'assets/images/left_to_right_arrow.png';
  static String imgPersonalFile = 'assets/images/personal_file.png';
  static String imgProfileData = 'assets/images/profile_data.png';
  static String imgRightToLeftArrow = 'assets/images/right_to_left_arrow.png';
  static String imgSwipeProfiles = 'assets/images/swipe_profiles.png';
  static String imgTask = 'assets/images/task.png';
}
