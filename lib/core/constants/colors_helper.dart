import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_mobile_index/core/constants/app_theme.dart';

class ColorsHelper {
  /// shade500
  static Color get primaryColor => Get.find<AppTheme>().primary;

  /// shade600
  static Color get primaryDarkColor => Get.find<AppTheme>().primaryDark;

  /// shade700
  static Color get primaryDarkerColor => Get.find<AppTheme>().primaryDarker;

  static Color get primary700Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade700;

  static Color get primary800Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade800;

  static Color get primary900Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade900;

  static Color get primary600Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade600;

  static Color get primary400Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade400;

  static Color get primary300Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade300;

  static Color get primary50Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade50;

  static Color get primary100Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade100;

  static Color get primary200Color =>
      Get.find<AppTheme>().primaryMaterialColor.shade200;

  static Color get primaryBgColor => Get.find<AppTheme>().primaryBg;

  static Color get accentColor => Get.find<AppTheme>().accent;

  static Color get secondaryColor => Get.find<AppTheme>().secondary;

  /// grey color shades
  /// shade600
  static Color get greyStrongColor => Get.find<AppTheme>().greyStrong;

  /// shade500
  static Color get greyColor => Get.find<AppTheme>().grey;

  /// shade400
  static Color get greyWeakColor => Get.find<AppTheme>().greyWeak;

  static Color get grey900Color =>
      Get.find<AppTheme>().greyMaterialColor.shade900;

  static Color get grey800Color =>
      Get.find<AppTheme>().greyMaterialColor.shade800;

  static Color get grey700Color =>
      Get.find<AppTheme>().greyMaterialColor.shade700;

  static Color get grey100Color =>
      Get.find<AppTheme>().greyMaterialColor.shade100;

  static Color get grey200Color =>
      Get.find<AppTheme>().greyMaterialColor.shade200;

  static Color get grey400Color =>
      Get.find<AppTheme>().greyMaterialColor.shade400;

  static Color get grey50Color =>
      Get.find<AppTheme>().greyMaterialColor.shade50;

  /// other app specific colors
  static Color get successColor => Get.find<AppTheme>().success;

  static Color get errorColor => Get.find<AppTheme>().error;

  static Color get warningColor => Get.find<AppTheme>().warning;

  static Color get transparentColor => Colors.transparent;

  static LinearGradient get gradientDark => Get.find<AppTheme>().gradientDark;

  static LinearGradient get gradientLight => Get.find<AppTheme>().gradientLight;

  static Color get borderColor => Get.find<AppTheme>().primary;

  static const Color readMeTextColor = Color(0xff4A5568);
  static const Color readMeButtonBorderColor = Color(0xffCBD5E0);
  static const Color readMeButtonTextColor = Color(0xff319795);
  static const Color tabSelectedColor = Color(0xff81E6D9);
  static const Color whiteColor = Color(0xffffffff);
  static const Color registrationTitleTextColor = Color(0xff2D3748);
  static const Color registrationTabNumberColor = Color(0xff718096);

  static LinearGradient get registrationGradient => const LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xFFEBF4FF),
          Color(0xFFE6FFFA),
        ],
      );

  static LinearGradient get registrationButtonGradient => const LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [
          Color(0xFF319795),
          Color(0xFF3182CE),
        ],
      );
}
