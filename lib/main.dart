import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:get/get.dart';
import 'package:test_mobile_index/core/constants/app_theme.dart';
import 'package:test_mobile_index/core/di/dependency_injection.dart';
import 'package:test_mobile_index/core/responsive_ui/responsive_wrapper.dart';
import 'package:test_mobile_index/core/routes/app_pages.dart';
import 'package:test_mobile_index/generated/l10n.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServiceLocator();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerStatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends ConsumerState<MyApp> {
  double width = 0.0;
  double height = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    Get.delete<AppTheme>();
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Builder(builder: (context) {
          final isWidescreen = !ResponsiveWrapper.of(context).isMobile;
          Get.put<AppTheme>(AppTheme.light(isWidescreen: isWidescreen),
              permanent: true);
          return GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus!.unfocus();
            },
            child: GetMaterialApp(
              onGenerateTitle: (ctx) => S.of(ctx).app_title,
              debugShowCheckedModeBanner: false,
              locale: Get.deviceLocale,
              theme: Get.find<AppTheme>().themeData,
              initialRoute: AppPages.initial,
              localizationsDelegates: const [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              getPages: AppPages.routes,
              onInit: () async {
                Get.put(Theme.of(context), permanent: true);
              },
            ),
          );
        }),
      );
}
