import 'package:flutter/material.dart';
import 'package:test_mobile_index/core/constants/colors_helper.dart';
import 'package:test_mobile_index/core/responsive_ui/responsive_stateless_widget.dart';

class ReadMeButtonWidget extends ResponsiveStatelessWidget {
  final String name;
  final Function onTap;

  const ReadMeButtonWidget({Key? key, required this.name, required this.onTap})
      : super(key: key);

  @override
  Widget? buildExpandedUI(BuildContext context) => buildWidget();

  @override
  Widget? buildCompactUI(BuildContext context) => buildWidget();

  Widget buildWidget() {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        height: 45,
        margin: const EdgeInsets.symmetric(vertical: 8),
        width: null,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: Border.all(color: ColorsHelper.readMeButtonBorderColor),
            borderRadius: BorderRadius.circular(12)),
        child: Text(
          name,
          style: const TextStyle(
              color: ColorsHelper.readMeButtonTextColor, fontSize: 14),
        ),
      ),
    );
  }
}
