import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:test_mobile_index/core/base/base_scaffold_screen_state.dart';
import 'package:test_mobile_index/core/constants/text_styles.dart';
import 'package:test_mobile_index/core/routes/app_pages.dart';
import 'package:test_mobile_index/core/ui/spacing.dart';
import 'package:test_mobile_index/generated/l10n.dart';
import 'package:test_mobile_index/ui/readme/widget/readme_button.dart';

class ReadMeScreen extends StatefulWidget {
  const ReadMeScreen({Key? key}) : super(key: key);

  @override
  ReadMeScreenState createState() => ReadMeScreenState();
}

class ReadMeScreenState extends BaseScaffoldScreenState<ReadMeScreen> {
  @override
  bool get showAppBar => false;

  @override
  Widget? buildExpandedBodyWidget() => buildWidget(false);

  @override
  Widget? buildCompactBodyWidget() => buildWidget(true);

  Widget buildWidget(bool isMobile) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      children: [
        const VSpace(35),
        RichText(
          text: TextSpan(
            style: AppTextStyles.readMeTextStyle,
            children: [
              TextSpan(text: S.of(context).read_me_desc_1),
              TextSpan(text: S.of(context).read_me_desc_2),
              TextSpan(text: S.of(context).read_me_desc_3),
              TextSpan(text: S.of(context).read_me_desc_4),
            ],
          ),
        ),
        ReadMeButtonWidget(
            name: S.of(context).mobile_view,
            onTap: () {
              Get.toNamed(Routes.registration,
                  arguments: S.of(context).mobile_view);
            }),
        ReadMeButtonWidget(
            name: S.of(context).desktop_view,
            onTap: () {
              Get.toNamed(Routes.registration,
                  arguments: S.of(context).desktop_view);
            }),
        ReadMeButtonWidget(
            name: S.of(context).desktop_scroll,
            onTap: () {
              Get.toNamed(Routes.registration,
                  arguments: S.of(context).desktop_scroll);
            }),
        const VSpace(25),
        Text(S.of(context).read_me_desc_5,
            style: AppTextStyles.readMeTextStyle),
        const VSpace(25),
      ],
    );
  }
}
