import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_mobile_index/core/base/base_scaffold_screen_state.dart';
import 'package:test_mobile_index/core/constants/assets_helper.dart';
import 'package:test_mobile_index/core/constants/colors_helper.dart';
import 'package:test_mobile_index/core/ui/spacing.dart';
import 'package:test_mobile_index/generated/l10n.dart';
import 'package:test_mobile_index/ui/registration/controller/registration_controller.dart';
import 'package:test_mobile_index/ui/registration/widgets/registration_button.dart';
import 'package:test_mobile_index/ui/registration/widgets/tab.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  RegistrationScreenState createState() => RegistrationScreenState();
}

class RegistrationScreenState
    extends BaseScaffoldScreenState<RegistrationScreen> {
  late RegistrationController registrationController;

  @override
  void initState() {
    super.initState();
    registrationController = Get.put(RegistrationController(context: context));
  }

  @override
  bool get showAppBar => false;

  @override
  Widget? buildExpandedBodyWidget() => buildWidget(false);

  @override
  Widget? buildCompactBodyWidget() => buildWidget(true);

  Widget buildWidget(bool isMobile) {
    //String string = ModalRoute.of(context)!.settings.arguments as String;

    return ListView(
      children: [
        (isMobile)
            ? Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        gradient: ColorsHelper.registrationGradient),
                    child: Column(
                      children: [
                        Text(S.of(context).deine_job_website,
                            style: const TextStyle(
                                fontSize: 42,
                                color:
                                    ColorsHelper.registrationTitleTextColor)),
                        Image.asset(AssetsHelper.imgAgreement),
                      ],
                    ),
                  ),
                  RegistrationButtonWidget(
                      onTap: () {}, margin: const EdgeInsets.all(20)),
                ],
              )
            : Container(
                padding: const EdgeInsets.symmetric(vertical: 40),
                alignment: Alignment.center,
                decoration:
                    BoxDecoration(gradient: ColorsHelper.registrationGradient),
                child: Row(
                  children: [
                    Flexible(flex: 3, child: Container()),
                    Flexible(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(S.of(context).deine_job_website,
                              style: const TextStyle(
                                  fontSize: 42,
                                  color:
                                      ColorsHelper.registrationTitleTextColor)),
                          const VSpace(20),
                          RegistrationButtonWidget(onTap: () {}),
                        ],
                      ),
                    ),
                    Flexible(flex: 1, child: Container()),
                    CircleAvatar(
                      backgroundColor: ColorsHelper.whiteColor,
                      radius: MediaQuery.of(context).size.width * 0.10,
                      backgroundImage: AssetImage(AssetsHelper.imgAgreement),
                    ),
                    Flexible(flex: 5, child: Container()),
                  ],
                ),
              ),
        Obx(() => TabWidget(
            onTap: (index) {
              registrationController.updateTabSelectedIndex(index);
            },
            selectedIndex: registrationController.tabSelectedIndex.value)),
        Obx(() {
          return Container(
            alignment: Alignment.center,
            child: Text(registrationController.getTabTitle,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontSize: 21, color: ColorsHelper.readMeTextColor)),
          );
        }),
        const VSpace(40),
        Obx(() {
          return Column(
            children: [
              Container(
                padding: const EdgeInsets.only(left: 20),
                width: MediaQuery.of(context).size.width,
                height: 250,
                child: Stack(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(flex: 5, child: Container()),
                        Image.asset(
                          registrationController.getTabImage1,
                          width: 220,
                        ),
                        Flexible(flex: 2, child: Container()),
                      ],
                    ),
                    Container(
                      alignment: Alignment.bottomLeft,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text("1.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 130,
                                  color:
                                      ColorsHelper.registrationTabNumberColor)),
                          Container(
                            padding:
                                const EdgeInsets.only(bottom: 30, left: 20),
                            child: Text(registrationController.getTabString1,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    fontSize: 16,
                                    color: ColorsHelper
                                        .registrationTabNumberColor)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 40, bottom: 25),
                decoration:
                    BoxDecoration(gradient: ColorsHelper.registrationGradient),
                width: MediaQuery.of(context).size.width,
                height: 300,
                child: Stack(
                  children: [
                    Container(
                      alignment: Alignment.bottomCenter,
                      child: Image.asset(
                        registrationController.getTabImage2,
                        width: 220,
                      ),
                    ),
                    Container(
                      alignment: (isMobile)
                          ? Alignment.topLeft
                          : Alignment.centerRight,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text("2.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 130,
                                  color:
                                      ColorsHelper.registrationTabNumberColor)),
                          Container(
                            padding:
                                const EdgeInsets.only(bottom: 50, left: 20),
                            child: Text(registrationController.getTabString2,
                                overflow: TextOverflow.clip,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    fontSize: 16,
                                    color: ColorsHelper
                                        .registrationTabNumberColor)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 60),
                width: MediaQuery.of(context).size.width,
                height: 270,
                child: Stack(
                  children: [
                    Container(
                      alignment: Alignment.bottomCenter,
                      child: Image.asset(
                        registrationController.getTabImage3,
                        width: 280,
                      ),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text("3.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 130,
                                  color:
                                      ColorsHelper.registrationTabNumberColor)),
                          Container(
                            padding: const EdgeInsets.only(bottom: 70),
                            child: Text(registrationController.getTabString3,
                                overflow: TextOverflow.clip,
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                    fontSize: 16,
                                    color: ColorsHelper
                                        .registrationTabNumberColor)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          );
        }),
        const VSpace(40),
      ],
    );
  }
}
