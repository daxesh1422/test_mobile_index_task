import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_mobile_index/core/constants/assets_helper.dart';
import 'package:test_mobile_index/generated/l10n.dart';

class RegistrationController extends GetxController {
  late BuildContext context;

  RegistrationController({required this.context});

  final tabSelectedIndex = 0.obs;

  updateTabSelectedIndex(int index) {
    tabSelectedIndex(index);
  }

  String get getTabTitle {
    switch (tabSelectedIndex.value) {
      case 0:
        return S.of(context).tab_0_title;
      case 2:
        return S.of(context).tab_2_title;
      case 1:
      default:
        return S.of(context).tab_1_title;
    }
  }

  String get getTabString1 {
    switch (tabSelectedIndex.value) {
      case 0:
        return S.of(context).tab_0_1;
      case 2:
        return S.of(context).tab_2_1;
      case 1:
      default:
        return S.of(context).tab_1_1;
    }
  }

  String get getTabString2 {
    switch (tabSelectedIndex.value) {
      case 0:
        return S.of(context).tab_0_2;
      case 2:
        return S.of(context).tab_2_2;
      case 1:
      default:
        return S.of(context).tab_1_2;
    }
  }

  String get getTabString3 {
    switch (tabSelectedIndex.value) {
      case 0:
        return S.of(context).tab_0_3;
      case 2:
        return S.of(context).tab_2_3;
      case 1:
      default:
        return S.of(context).tab_1_3;
    }
  }

  String get getTabImage1 {
    return AssetsHelper.imgProfileData;
  }

  String get getTabImage2 {
    switch (tabSelectedIndex.value) {
      case 0:
        return AssetsHelper.imgTask;
      case 2:
        return AssetsHelper.imgJobOffers;
      case 1:
      default:
        return AssetsHelper.imgAboutMe;
    }
  }

  String get getTabImage3 {
    switch (tabSelectedIndex.value) {
      case 0:
        return AssetsHelper.imgPersonalFile;
      case 2:
        return AssetsHelper.imgBusinessDeal;
      case 1:
      default:
        return AssetsHelper.imgSwipeProfiles;
    }
  }
}
