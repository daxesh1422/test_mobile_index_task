import 'package:flutter/material.dart';
import 'package:test_mobile_index/core/constants/colors_helper.dart';
import 'package:test_mobile_index/core/responsive_ui/responsive_stateless_widget.dart';
import 'package:test_mobile_index/generated/l10n.dart';

// ignore: must_be_immutable
class TabWidget extends ResponsiveStatelessWidget {
  final Function(int) onTap;
  int selectedIndex = 1;

  TabWidget({Key? key, required this.onTap, this.selectedIndex = 1})
      : super(key: key);

  @override
  Widget? buildExpandedUI(BuildContext context) => buildWidget(context, false);

  @override
  Widget? buildCompactUI(BuildContext context) => buildWidget(context, true);

  Widget buildWidget(BuildContext context, bool isMobile) {
    return Container(
      height: 45,
      margin: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          tabWidget(context, S.of(context).arbeitnehmer, 0, isMobile),
          tabWidget(context, S.of(context).arbeitgeber, 1, isMobile),
          tabWidget(context, S.of(context).tempor, 2, isMobile),
        ],
      ),
    );
  }

  Widget tabWidget(
      BuildContext context, String name, int index, bool isMobile) {
    return Flexible(
      flex: 1,
      child: GestureDetector(
        onTap: () {
          onTap(index);
        },
        child: Container(
          height: 45,
          width: (!isMobile)?150:null,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: (index == selectedIndex)
                  ? ColorsHelper.tabSelectedColor
                  : null,
              border: Border.all(
                  color: (index == selectedIndex)
                      ? ColorsHelper.tabSelectedColor
                      : ColorsHelper.readMeButtonBorderColor),
              borderRadius: borderRadius(index)),
          child: Text(name,
              style: TextStyle(
                  color: (index == selectedIndex)
                      ? ColorsHelper.whiteColor
                      : ColorsHelper.readMeButtonTextColor,
                  fontSize: 14)),
        ),
      ),
    );
  }

  BorderRadiusGeometry borderRadius(int index) {
    switch (index) {
      case 0:
        return const BorderRadius.only(
          topLeft: Radius.circular(12),
          bottomLeft: Radius.circular(12),
        );
      case 2:
        return const BorderRadius.only(
          topRight: Radius.circular(12),
          bottomRight: Radius.circular(12),
        );
      case 1:
      default:
        return BorderRadius.circular(0);
    }
  }
}
