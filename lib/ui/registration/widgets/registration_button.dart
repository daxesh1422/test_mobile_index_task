import 'package:flutter/material.dart';
import 'package:test_mobile_index/core/constants/colors_helper.dart';
import 'package:test_mobile_index/core/responsive_ui/responsive_stateless_widget.dart';
import 'package:test_mobile_index/generated/l10n.dart';

class RegistrationButtonWidget extends ResponsiveStatelessWidget {
  final Function onTap;
  final EdgeInsetsGeometry? margin;

  const RegistrationButtonWidget({Key? key, required this.onTap, this.margin})
      : super(key: key);

  @override
  Widget? buildExpandedUI(BuildContext context) => buildWidget(context);

  @override
  Widget? buildCompactUI(BuildContext context) => buildWidget(context);

  Widget buildWidget(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        height: 45,
        margin: margin,
        width: null,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            gradient: ColorsHelper.registrationButtonGradient,
            border: Border.all(color: ColorsHelper.readMeButtonBorderColor),
            borderRadius: BorderRadius.circular(12)),
        child: Text(
          S.of(context).kostenlos_registrieren,
          textAlign: TextAlign.center,
          style: const TextStyle(color: ColorsHelper.whiteColor, fontSize: 14),
        ),
      ),
    );
  }
}
