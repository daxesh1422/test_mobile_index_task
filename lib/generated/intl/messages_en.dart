// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "app_title": MessageLookupByLibrary.simpleMessage("Test Index"),
        "arbeitgeber": MessageLookupByLibrary.simpleMessage("Arbeitgeber"),
        "arbeitnehmer": MessageLookupByLibrary.simpleMessage("Arbeitnehmer"),
        "deine_job_website":
            MessageLookupByLibrary.simpleMessage("Deine Job\nwebsite"),
        "desktop_scroll":
            MessageLookupByLibrary.simpleMessage("Desktop scroll"),
        "desktop_view": MessageLookupByLibrary.simpleMessage("Desktop view"),
        "kostenlos_registrieren":
            MessageLookupByLibrary.simpleMessage("Kostenlos Registrieren"),
        "mobile_view": MessageLookupByLibrary.simpleMessage("Mobile view"),
        "read_me_desc_1": MessageLookupByLibrary.simpleMessage(
            "yeah you rocks👏, now you\'re in the next level.\n\n"),
        "read_me_desc_2": MessageLookupByLibrary.simpleMessage(
            "Now you can create the test page and show how skilled you are.\n - Only frontend\n - With Flutter\n\n"),
        "read_me_desc_3": MessageLookupByLibrary.simpleMessage(
            "Create this test page and push it to GitLab.\n\n"),
        "read_me_desc_4": MessageLookupByLibrary.simpleMessage(
            "Send me via Slack a message when you will start with the test page and when you will be finished.\n"),
        "read_me_desc_5": MessageLookupByLibrary.simpleMessage(
            "The website can be scrolled, while the button \"Kostenlos Registrieren\" goes on the top bar."),
        "tab_0_1":
            MessageLookupByLibrary.simpleMessage("Erstellen dein Lebenslauf"),
        "tab_0_2":
            MessageLookupByLibrary.simpleMessage("Erstellen dein Lebenslauf"),
        "tab_0_3": MessageLookupByLibrary.simpleMessage(
            "Mit nur einem Klick\nbewerben"),
        "tab_0_title": MessageLookupByLibrary.simpleMessage(
            "Drei einfache Schritte\nzu deinem neuen Job"),
        "tab_1_1":
            MessageLookupByLibrary.simpleMessage("Erstellen dein Lebenslauf"),
        "tab_1_2":
            MessageLookupByLibrary.simpleMessage("Erstellen dein Lebenslauf"),
        "tab_1_3": MessageLookupByLibrary.simpleMessage(
            "Wähle deinen\nneuen Mitarbeiter aus"),
        "tab_1_title": MessageLookupByLibrary.simpleMessage(
            "Drei einfache Schritte\nzu deinem neuen Mitarbeiter"),
        "tab_2_1":
            MessageLookupByLibrary.simpleMessage("Erstellen dein Lebenslauf"),
        "tab_2_2": MessageLookupByLibrary.simpleMessage(
            "Erhalte Vermittlungs\nangebot von Arbeitgeber"),
        "tab_2_3": MessageLookupByLibrary.simpleMessage(
            "Vermittlung nach\nProvision oder\nStundenlohn"),
        "tab_2_title": MessageLookupByLibrary.simpleMessage(
            "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter"),
        "tempor": MessageLookupByLibrary.simpleMessage("Temporärbüro")
      };
}
