// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Test Index`
  String get app_title {
    return Intl.message(
      'Test Index',
      name: 'app_title',
      desc: '',
      args: [],
    );
  }

  /// `yeah you rocks👏, now you're in the next level.\n\n`
  String get read_me_desc_1 {
    return Intl.message(
      'yeah you rocks👏, now you\'re in the next level.\n\n',
      name: 'read_me_desc_1',
      desc: '',
      args: [],
    );
  }

  /// `Now you can create the test page and show how skilled you are.\n - Only frontend\n - With Flutter\n\n`
  String get read_me_desc_2 {
    return Intl.message(
      'Now you can create the test page and show how skilled you are.\n - Only frontend\n - With Flutter\n\n',
      name: 'read_me_desc_2',
      desc: '',
      args: [],
    );
  }

  /// `Create this test page and push it to GitLab.\n\n`
  String get read_me_desc_3 {
    return Intl.message(
      'Create this test page and push it to GitLab.\n\n',
      name: 'read_me_desc_3',
      desc: '',
      args: [],
    );
  }

  /// `Send me via Slack a message when you will start with the test page and when you will be finished.\n`
  String get read_me_desc_4 {
    return Intl.message(
      'Send me via Slack a message when you will start with the test page and when you will be finished.\n',
      name: 'read_me_desc_4',
      desc: '',
      args: [],
    );
  }

  /// `The website can be scrolled, while the button "Kostenlos Registrieren" goes on the top bar.`
  String get read_me_desc_5 {
    return Intl.message(
      'The website can be scrolled, while the button "Kostenlos Registrieren" goes on the top bar.',
      name: 'read_me_desc_5',
      desc: '',
      args: [],
    );
  }

  /// `Mobile view`
  String get mobile_view {
    return Intl.message(
      'Mobile view',
      name: 'mobile_view',
      desc: '',
      args: [],
    );
  }

  /// `Desktop view`
  String get desktop_view {
    return Intl.message(
      'Desktop view',
      name: 'desktop_view',
      desc: '',
      args: [],
    );
  }

  /// `Desktop scroll`
  String get desktop_scroll {
    return Intl.message(
      'Desktop scroll',
      name: 'desktop_scroll',
      desc: '',
      args: [],
    );
  }

  /// `Deine Job\nwebsite`
  String get deine_job_website {
    return Intl.message(
      'Deine Job\nwebsite',
      name: 'deine_job_website',
      desc: '',
      args: [],
    );
  }

  /// `Kostenlos Registrieren`
  String get kostenlos_registrieren {
    return Intl.message(
      'Kostenlos Registrieren',
      name: 'kostenlos_registrieren',
      desc: '',
      args: [],
    );
  }

  /// `Arbeitnehmer`
  String get arbeitnehmer {
    return Intl.message(
      'Arbeitnehmer',
      name: 'arbeitnehmer',
      desc: '',
      args: [],
    );
  }

  /// `Arbeitgeber`
  String get arbeitgeber {
    return Intl.message(
      'Arbeitgeber',
      name: 'arbeitgeber',
      desc: '',
      args: [],
    );
  }

  /// `Temporärbüro`
  String get tempor {
    return Intl.message(
      'Temporärbüro',
      name: 'tempor',
      desc: '',
      args: [],
    );
  }

  /// `Drei einfache Schritte\nzu deinem neuen Job`
  String get tab_0_title {
    return Intl.message(
      'Drei einfache Schritte\nzu deinem neuen Job',
      name: 'tab_0_title',
      desc: '',
      args: [],
    );
  }

  /// `Drei einfache Schritte\nzu deinem neuen Mitarbeiter`
  String get tab_1_title {
    return Intl.message(
      'Drei einfache Schritte\nzu deinem neuen Mitarbeiter',
      name: 'tab_1_title',
      desc: '',
      args: [],
    );
  }

  /// `Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter`
  String get tab_2_title {
    return Intl.message(
      'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter',
      name: 'tab_2_title',
      desc: '',
      args: [],
    );
  }

  /// `Erstellen dein Lebenslauf`
  String get tab_0_1 {
    return Intl.message(
      'Erstellen dein Lebenslauf',
      name: 'tab_0_1',
      desc: '',
      args: [],
    );
  }

  /// `Erstellen dein Lebenslauf`
  String get tab_0_2 {
    return Intl.message(
      'Erstellen dein Lebenslauf',
      name: 'tab_0_2',
      desc: '',
      args: [],
    );
  }

  /// `Mit nur einem Klick\nbewerben`
  String get tab_0_3 {
    return Intl.message(
      'Mit nur einem Klick\nbewerben',
      name: 'tab_0_3',
      desc: '',
      args: [],
    );
  }

  /// `Erstellen dein Lebenslauf`
  String get tab_1_1 {
    return Intl.message(
      'Erstellen dein Lebenslauf',
      name: 'tab_1_1',
      desc: '',
      args: [],
    );
  }

  /// `Erstellen dein Lebenslauf`
  String get tab_1_2 {
    return Intl.message(
      'Erstellen dein Lebenslauf',
      name: 'tab_1_2',
      desc: '',
      args: [],
    );
  }

  /// `Wähle deinen\nneuen Mitarbeiter aus`
  String get tab_1_3 {
    return Intl.message(
      'Wähle deinen\nneuen Mitarbeiter aus',
      name: 'tab_1_3',
      desc: '',
      args: [],
    );
  }

  /// `Erstellen dein Lebenslauf`
  String get tab_2_1 {
    return Intl.message(
      'Erstellen dein Lebenslauf',
      name: 'tab_2_1',
      desc: '',
      args: [],
    );
  }

  /// `Erhalte Vermittlungs\nangebot von Arbeitgeber`
  String get tab_2_2 {
    return Intl.message(
      'Erhalte Vermittlungs\nangebot von Arbeitgeber',
      name: 'tab_2_2',
      desc: '',
      args: [],
    );
  }

  /// `Vermittlung nach\nProvision oder\nStundenlohn`
  String get tab_2_3 {
    return Intl.message(
      'Vermittlung nach\nProvision oder\nStundenlohn',
      name: 'tab_2_3',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
